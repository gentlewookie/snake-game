package org.snake.types;

import javafx.scene.paint.*;

/**
 * Created by Batiskaf on 17.04.2016.
 */
public enum NodeName {

    SNAKE(Color.YELLOW),
    FROG(Color.GREEN);

    private Color color;

    NodeName(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

}
