package org.snake;

import javafx.application.Application;
import javafx.stage.Stage;
import org.snake.view.ViewManager;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        ViewManager.getInstance().setPrimaryStage(primaryStage);
        ViewManager.getInstance().createConfig();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
