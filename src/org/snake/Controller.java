package org.snake;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Rectangle;
import org.snake.types.NodeName;
import org.snake.view.NodeFactory;
import org.snake.view.ViewManager;
import java.util.List;

public class Controller {
    private static final Integer CELL = 10;

    @FXML public TextField snakeLength;
    @FXML public TextField width;
    @FXML public TextField height;
    @FXML public TextField frogs;
    @FXML public BorderPane gamePane;
    @FXML public Group fieldGroup;

    private Integer intWidth = 20 * CELL;
    private Integer intHeight = 20 * CELL;
    private Integer intSnakeLength = 3;
    private Integer intFrogs = 1;

    private static final NodeFactory nodeFactory = new NodeFactory();
    private List<Rectangle> snakeNodes;

    @FXML
    public void submit(ActionEvent actionEvent) {
        intHeight = Integer.parseInt(height.getText()) * CELL;
        intWidth = Integer.parseInt(width.getText()) * CELL;
        intSnakeLength = Integer.parseInt(snakeLength.getText());
        intFrogs = Integer.parseInt(frogs.getText());
    }

    @FXML
    public void createGame(ActionEvent actionEvent) throws Exception {
        ViewManager.getInstance().createGame(intWidth, intHeight);
    }

    @FXML
    public void start(ActionEvent actionEvent) {
        createSnake();
        createFrogs();

    }

    @FXML
    public void stop(ActionEvent actionEvent) {

    }

    @FXML
    public void closeGame(ActionEvent actionEvent) throws Exception {
        stop(actionEvent);
        ViewManager.getInstance().createConfig();
    }


    public void createSnake() {
        for (int i = 0; i < intSnakeLength; i++) {
            fieldGroup.getChildren().
                    add(nodeFactory.getNode(NodeName.SNAKE, CELL));
        }
    }

    public void createFrogs() {
        for (int i = 0; i < intFrogs; i++) {
            fieldGroup.getChildren().
                    add(nodeFactory.getNode(NodeName.FROG, CELL));
        }
    }



}
