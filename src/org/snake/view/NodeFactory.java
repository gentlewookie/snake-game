package org.snake.view;

import javafx.scene.shape.Rectangle;
import org.snake.types.NodeName;

/**
 * Created by Batiskaf on 17.04.2016.
 */
public class NodeFactory {

    public Rectangle getNode(NodeName obj, int cell){
        Rectangle rectangle = new Rectangle(cell, cell);
        rectangle.setFill(obj.getColor());
        return rectangle;
    }
}
