package org.snake.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

/**
 * Created by Batiskaf on 13.04.2016.
 */
public class ViewManager {
    private static Stage primaryStage;

    private ViewManager() {
    }

    private static class SingletonHolder {
        private final static ViewManager instance = new ViewManager();
    }

    public static ViewManager getInstance() {
        return SingletonHolder.instance;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void createConfig() throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("config.fxml"));
        primaryStage.setTitle("Snake config");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public void createGame(Integer width, Integer height) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("game.fxml"));
        primaryStage.setTitle("Snake game");
        primaryStage.setScene(new Scene(root,width,height));
        primaryStage.show();
    }

}
